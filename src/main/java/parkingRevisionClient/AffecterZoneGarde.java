package parkingRevisionClient;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import entities.Poste;
import service.MyServiceRemote;

public class AffecterZoneGarde {

	public static void main(String[] args) throws NamingException {
		InitialContext cx= new InitialContext();
		Object obj= cx.lookup("/parkingRevision-ear/parkingRevision-ejb/MyService!service.MyServiceRemote");
		MyServiceRemote proxy= (MyServiceRemote) obj;
		
		proxy.affecterPersonnelZone(4, 3, Poste.GARDE_JOUR);
		proxy.affecterPersonnelZone(4, 1, Poste.RESPONSABLE);
	}

}
