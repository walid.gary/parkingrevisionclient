package parkingRevisionClient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import entities.Personnel;
import entities.Poste;
import service.MyServiceRemote;


public class AjoutPersonnel {
	public static void main(String[] args) throws NamingException, ParseException {
		// TODO Auto-generated method stub
		InitialContext cx= new InitialContext();
		Object obj= cx.lookup("/parkingRevision-ear/parkingRevision-ejb/MyService!service.MyServiceRemote");
		MyServiceRemote proxy= (MyServiceRemote) obj;
		Personnel p = new Personnel();
		p.setAge(35);
		SimpleDateFormat df= new SimpleDateFormat("dd/MM/yyyy");
		Date dd=df.parse("02/05/2015");
		p.setDateDeRecrutement(dd);
		p.setNom("Ammar");
		p.setPrenom("Med");
		p.setLogin("Med");
		p.setPassword("Med");
		p.setPoste(Poste.RESPONSABLE);
		
		Personnel pp = new Personnel();
		pp.setAge(32);
		Date ddd=df.parse("01/05/2017");
		pp.setDateDeRecrutement(ddd);
		pp.setNom("Omar");
		pp.setPrenom("Ahmed");
		pp.setLogin("Ahmed");
		pp.setPassword("Ahmed");
		pp.setPoste(Poste.GARDE_JOUR);
		
		Personnel ppp = new Personnel();
		ppp.setAge(30);
		Date dddd=df.parse("02/08/2017");
		ppp.setDateDeRecrutement(dddd);
		ppp.setNom("Khammasi");
		ppp.setPrenom("Firas");
		ppp.setLogin("Firas");
		ppp.setPassword("Firas");
		ppp.setPoste(Poste.GARDE_JOUR);
		
		Personnel pppp = new Personnel();
		pppp.setAge(28);
		Date ddddd=df.parse("22/06/2017");
		pppp.setDateDeRecrutement(ddddd);
		pppp.setNom("Hamed");
		pppp.setPrenom("Houssem");
		pppp.setLogin("Hamed");
		pppp.setPassword("Hmaed");
		pppp.setPoste(Poste.GARDE_NUIT);
		
		proxy.ajouterPersonnel(p);
		proxy.ajouterPersonnel(pp);
		proxy.ajouterPersonnel(ppp);
		proxy.ajouterPersonnel(pppp);
	}
}
