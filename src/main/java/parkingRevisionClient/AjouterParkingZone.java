package parkingRevisionClient;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import entities.Parking;
import entities.Zone;
import service.MyServiceRemote;

public class AjouterParkingZone {
	public static void main(String[] args) throws NamingException{
		// TODO Auto-generated method stub
		InitialContext cx= new InitialContext();
		Object obj= cx.lookup("/parkingRevision-ear/parkingRevision-ejb/MyService!service.MyServiceRemote");
		MyServiceRemote proxy= (MyServiceRemote) obj;
		Parking p = new Parking();
		p.setAdresse("Mnihla");
		p.setCapacite(500);
		p.setDesignation("G�ant");
		
		Zone z = new Zone();
		z.setRef("Z01");
		z.setDimension(200.3f);
		
		Zone zz = new Zone();
		zz.setRef("Z02");
		zz.setDimension(122f);
		
		Zone zzz = new Zone();
		zzz.setRef("Z03");
		zzz.setDimension(250.6f);
		
		Zone zzzz = new Zone();
		zzzz.setRef("Z04");
		zzzz.setDimension(189.5f);
		
		List <Zone> ls = new ArrayList<Zone>();
		ls.add(z);ls.add(zz);ls.add(zzz);ls.add(zzzz);
		proxy.ajoutParkingetZones(p, ls);
	}
}
