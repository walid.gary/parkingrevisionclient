package parkingRevisionClient;

import java.util.ArrayList;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import entities.Personnel;
import entities.Zone;
import service.MyServiceRemote;

public class ListerAllPersonnels {

	public static void main(String[] args) throws NamingException {
		InitialContext cx= new InitialContext();
		Object obj= cx.lookup("/parkingRevision-ear/parkingRevision-ejb/MyService!service.MyServiceRemote");
		MyServiceRemote proxy= (MyServiceRemote) obj;
		List <Personnel> ls = new ArrayList<Personnel>();
		ls=proxy.listerPersonnel();
		System.out.println("Personnels : ");
		for(Personnel p : ls) {
			System.out.println(p);
		}

	}

}
